<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('fint_id')->nullable();
            $table->enum('fint_category', array('BORROWER', 'LENDER'))->nullable();
            $table->mediumText('feedback');
            $table->enum('category', array('SUPPORT', 'TECH', 'ENQUIRY'));
            $table->integer('created_by')->unsigned()->nullable();
            $table->dateTimeTz('date_opened');
            $table->integer('resolved_by')->unsigned()->nullable();
            $table->enum('resolved', array('TRUE', 'FALSE'));
            $table->dateTimeTz('date_closed')->nullable();
            $table->enum('priority', array('1', '2', '3'));
            $table->timestamps();

//            $table->foreign('created_by')
//                ->references('id')->on('users')
//                ->onDelete('set null');
//            $table->foreign('resolved_by')
//                ->references('id')->on('users')
//                ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks');
    }
}
