<?php

namespace Customercare;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'firstname', 'lastname', 'email', 'username', 'email', 'password', ''
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Get the feedbacks created by the user.
     */
    public function createdFeedbacks()
    {
        return $this->hasMany('Customercare\Feedback', 'created_by');
    }

    /**
     * Get the user that resolved the feedback.
     */
    public function resolvedFeedbacks()
    {
        return $this->hasMany('Customercare\Feedback', 'resolved_by');
    }

    /**
     * Get the user that resolved the feedback.
     */
    public function logs()
    {
        return $this->hasMany('Customercare\Log', 'user_id');
    }



}
