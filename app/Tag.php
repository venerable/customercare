<?php

namespace Customercare;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];


    /**
     * Get the feedbacks associated with the tag.
     */
    public function feedbacks()
    {
        return $this->belongsToMany('Customercare\Feedback');
    }
}
