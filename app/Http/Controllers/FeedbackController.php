<?php

namespace Customercare\Http\Controllers;

use Customercare\Feedback;
use Customercare\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Feedback::orderBy('created_at', 'desc')
            ->latest()
            ->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entry = null;

        DB::transaction(function () use ($request, &$entry) {
            $request->validate([
                'phone' => 'nullable|digits_between:9,11',
                'name' => 'required|string',
                'feedback' => 'required|string',
                'category' => 'required|in:SUPPORT,TECH,ENQUIRY',
                'priority' => 'required|in:NORMAL,HIGH',
            ]);

            $date = date('Y-m-d H:i:s');

            $entry = new Feedback;
            $entry->name = $request->name;
            $entry->phone = $request->phone;
            $entry->fint_id = $request->fint_id;
            $entry->fint_category = $request->fint_category;
            $entry->feedback = $request->feedback;
            $entry->category = $request->category;
            $entry->created_by = Auth::user()->id;
            $entry->date_opened = $date;
            $entry->priority = $request->priority;
            if ($request->resolved == TRUE) {
                $entry->resolved_by = Auth::user()->id;
                $entry->date_closed = $date;
            }

            $tags = $request->tags;

            $feedback = Auth::user()->createdFeedbacks()->save($entry);
            $feedback->tags()->attach($tags);

            $log = new Log;
            $log->action = 'INSERT';
            $log->feedback_id = $feedback->id;
            $log->user_id = Auth::user()->id;
            $log->date = $date;

            Auth::user()->logs()->save($log);

        });

        return response()->json(['entry' => $entry]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Feedback::with('tags')->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $request->validate([
                'phone' => 'required|digits_between:9,11',
                'name' => 'required|string',
                'feedback' => 'required|string',
                'category' => 'required|in:SUPPORT,TECH,ENQUIRY',
                'priority' => 'required|in:NORMAL,HIGH',
            ]);

            $date = date('Y-m-d H:i:s');

            $entry = new Feedback;
            $entry->name = $request->name;
            $entry->phone = $request->phone;
            $entry->feedback = $request->feedback;
            $entry->category = $request->category;
            $entry->priority = $request->priority;
            if ($request->resolved == TRUE) {
                $entry->resolved_by = Auth::user()->id;
                $entry->date_closed = $date;
            }

            $tags = $request->tags;

            Feedback::findOrFail($id)->update($entry->toArray());
            Feedback::findOrFail($id)->tags()->sync($tags);

            $log = new Log;
            $log->action = 'UPDATE';
            $log->feedback_id = $id;
            $log->user_id = Auth::user()->id;
            $log->date = $date;

            Auth::user()->logs()->save($log);
        });
    }

    /**
     * Resolve the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function resolve($id)
    {
        DB::transaction(function () use ($id) {
            $date = date('Y-m-d H:i:s');

            Feedback::findOrFail($id)->update([
                'resolved_by' => Auth::user()->id,
                'date_closed' => $date
            ]);

            $log = new Log;
            $log->action = 'RESOLVE';
            $log->feedback_id = $id;
            $log->user_id = Auth::user()->id;
            $log->date = $date;

            Auth::user()->logs()->save($log);

        });
    }

    /**
     * Resolve the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function unresolve($id)
    {
        DB::transaction(function () use ($id) {
            $date = date('Y-m-d H:i:s');

            Feedback::findOrFail($id)->update([
                'resolved_by' => null,
                'date_closed' => null
            ]);

            $log = new Log;
            $log->action = 'UNRESOLVE';
            $log->feedback_id = $id;
            $log->user_id = Auth::user()->id;
            $log->date = $date;

            Auth::user()->logs()->save($log);

        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $feedback = Feedback::destroy($id);

            $date = date('Y-m-d H:i:s');

            $log = new Log;
            $log->action = 'DELETE';
            $log->feedback_id = $id;
            $log->user_id = Auth::user()->id;
            $log->date = $date;

            Auth::user()->logs()->save($log);

        });
    }

    /**
     * Display a filtered listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        return Feedback::phone($request->phone)
            ->joinTagTable($request->tag)
            ->category($request->category)
            ->priority($request->priority)
            ->resolved($request->resolved)
            ->fintCategory($request->fint_category)
//            ->tag($request->tag)
            ->latest()
            ->get();
    }
}
