<?php

namespace Customercare;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';

    protected $guarded = [];

    /**
     * Get the user that created the feedback.
     */
    public function creator()
    {
        return $this->belongsTo('Customercare\User', 'created_by');
    }

    /**
     * Get the user that resolved the feedback.
     */
    public function resolver()
    {
        return $this->belongsTo('Customercare\User', 'resolved_by');
    }

    /**
     * Get the tags associated with the feedback.
     */
    public function tags()
    {
        return $this->belongsToMany('Customercare\Tag');
    }

    /**
     * Get the logs associated with the feedback.
     */
    public function logs()
    {
        return $this->hasMany('Customercare\Log', 'feedback_id');
    }

    public function scopePhone($query, $phone)
    {
        if (empty($phone)) {
            return $query;
        }

        return $query->where('feedbacks.phone', $phone);
    }

    public function scopeCategory($query, $category)
    {
        if (empty($category)) {
            return $query;
        }

        return $query->where('feedbacks.category', $category);
    }

    public function scopePriority($query, $priority)
    {
        if (empty($priority)) {
            return $query;
        }

        return $query->where('feedbacks.priority', $priority);
    }

    public function scopeResolved($query, $resolved)
    {
        if ($resolved == 'false') {
            return $query->where('feedbacks.resolved_by', '=', null);
        }

        return $query->where('feedbacks.resolved_by', '!=', null);
    }

    public function scopeJoinTagTable($query, $tag) {
        return $query->leftJoin('feedback_tag', function ($join) use ($tag) {
            $join->on('feedbacks.id', '=', 'feedback_tag.feedback_id')
                ->where('feedback_tag.tag_id', '=', $tag);
            });
    }

    public function scopeTag($query, $tag)
    {
        if (empty($tag)) {
            return $query;
        }

        return $query->where('feedback_tag.tag_id', '=', $tag);
    }

    public function scopeFintCategory($query, $fint_category)
    {
        if (empty($fint_category)) {
            return $query;
        }

        return $query->where('feedbacks.fint_category', $fint_category);
    }
}
