<?php

namespace Customercare;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    protected $table = 'logs';

    /**
     * Get the user that has the log.
     */
    public function owner()
    {
        return $this->belongsTo('Customercare\User', 'user_id');
    }

    /**
     * Get the feedback that has the log.
     */
    public function feedback()
    {
        return $this->belongsTo('Customercare\Feedback', 'feedback_id');
    }
}
