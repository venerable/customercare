<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('entries', 'FeedbackController@index');

Route::get('entries/filter', 'FeedbackController@filter');

Route::get('entry/{id}', 'FeedbackController@show');

Route::post('entry/store', 'FeedbackController@store');

Route::patch('entry/edit/{id}', 'FeedbackController@update');

Route::patch('entry/resolve/{id}', 'FeedbackController@resolve');

Route::patch('entry/unresolve/{id}', 'FeedbackController@unresolve');

Route::delete('entry/{id}', 'FeedbackController@destroy');
