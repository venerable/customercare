<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['middleware' => 'auth:api'], function() {

    Route::get('entries', function() {
        return Customercare\Feedback::latest()->orderBy('created_at', 'desc')->get();
    });

    Route::get('entries/{id}', function($id) {
        return Customercare\Feedback::findOrFail($id);
    });

//    Route::post('entries/store', 'FeedbackController@store');

    Route::post('entries/store', function() {
        return \Illuminate\Support\Facades\Auth::user();
    });


//
//    Route::patch('task/{id}', function(Request $request, $id) {
//        Customercare\Task::findOrFail($id)->update(['body' => $request->input(['body'])]);
//    });
//
//    Route::delete('task/{id}', function($id) {
//        return Customercare\Task::destroy($id);
//    });
});