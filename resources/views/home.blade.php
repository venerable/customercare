<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <script>window.Laravel = {csrfToken: '{{ csrf_token() }}'}</script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Customer care') }}</title>

    <!-- Styles -->
    <link rel="shortcut icon" href=""/>
    <link rel="icon" type="image/png" href="">
    <link rel="apple-touch-icon" href="">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('css/parsley.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/smt-bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/nprogress.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/formstyles.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/toastr.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
</head>
<body>

<div id="app" v-cloak class="container">
    <div class="row">
        <h2 class="pull-left">Hello, {{ucfirst($firstname)}}</h2>
        <a href="{{ route('logout') }}" class="pull-right btn btn-danger"
           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
    <hr/>

    <ul class="nav nav-tabs tabs-material">
        <li class="active"><a id="tab1_a" href="#tab1" data-toggle="tab">Insert Feedback</a></li>
        <li><a href="#tab2" data-toggle="tab">View All Feedbacks</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="tab1">
            <div class="row">
                <div class="col-sm-8">
                    <form id="feedbackForm" @submit.prevent="edit ? updateEntry(entry.id) : createEntry()">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="control-label">Name <span style="color: red">*</span></label>
                            <input class="form-control" v-model="entry.name" id="name" name="name" type="text"
                                   value="Ugo Opara"
                                   placeholder="Enter last name" data-parsley-required/>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="control-label">Phone</label>
                            <input class="form-control" v-model="entry.phone" id="phone" name="phone" value=""
                                   placeholder="Enter phone number"
                                   data-parsley-minlength="9" data-parsley-maxlength="11"
                                   data-parsley-type="digits" data-parsley-minlength-message="Minimum of 9 digits"
                                   data-parsley-maxlength-message="Maximum of 11 digits"
                                   data-parsley-type-message="This value should contain digits only"/>
                        </div>

                        <div class="form-group">
                            <label for="fint_id" class="control-label">FINT ID </label>
                            <input class="form-control" v-model="entry.fint_id" id="fint_id" name="fint_id"
                                   type="text" placeholder="Enter FINT ID if any"/>
                        </div>

                        <div class="form-group">
                            <label for="fint_category" class="control-label">FINT Category </label>
                            <select class="form-control" v-model="entry.fint_category" id="fint_category"
                                    name="fint_category" placeholder="Borrower or Lender">
                                <option value="BORROWER">Borrower</option>
                                <option value="LENDER">Lender</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="feedback" class="control-label">Feedback (not more than 200 words) <span
                                        style="color: red">*</span> </label>
                            <textarea class="form-control" v-model="entry.feedback" id="feedback" name="feedback"
                                      type="text"
                                      placeholder="Enter description" data-parsley-required></textarea>
                        </div>

                        <div class="form-group">
                            <label for="category" class="control-label">Category <span style="color: red">*</span>
                            </label>
                            <select class="form-control" v-model="entry.category" id="category" name="category"
                                    data-parsley-required>
                                <option value="SUPPORT">Support</option>
                                <option value="TECH">Tech</option>
                                <option value="ENQUIRY">Enquiry</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="priority" class="control-label">Priority <span style="color: red">*</span>
                            </label>
                            <p class="btn-group radio-group radio-group-success">
                                <input id="r_normal" name="priority" value="NORMAL" v-model="entry.priority"
                                       type="radio">
                                <label for="r_normal" class="btn">Normal</label>
                                <input id="r_high" name="priority" value="HIGH" v-model="entry.priority" type="radio">
                                <label for="r_high" class="btn">High</label>
                            </p>
                        </div>

                        <div class="form-group">
                            <label for="tags" class="control-label">Tags </label>
                            <select2 :options="options" v-model="entry.tags">
                                @foreach ($tags as $tag)
                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                @endforeach
                            </select2>
                        </div>

                        <div class="form-group">
                            {{--<label for="resolved" class="control-label">Resolved</label>--}}
                            <div class="switch switch-line">
                                <input id="resolved" name="resolved" v-model="entry.resolved" type="checkbox">
                                <label for="resolved" class="control-label">Resolved</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </form>
                </div>

                <div class="col-sm-4">
                    <h3 style="padding-left: 10px;">Recent Entries</h3>
                    <ul>
                        <li v-for="entry in entries">
                            <p>@{{entry.feedback | shorten}} &nbsp;&nbsp;
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="tab2">

            <h2>All Entries</h2>

            <div class="panel-group panel-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                               aria-expanded="true" aria-controls="collapseOne" class="">
                                Filter
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingOne" aria-expanded="true" style="">
                        <div class="panel-body">
                            <form id="filter-form" @submit.prevent="filterEntries()">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="phone_f" class="control-label ">Phone </label>
                                            <input class="form-control " v-model="filter.phone" id="phone_f"
                                                   name="phone" value=""
                                                   placeholder="Enter phone number"
                                                   data-parsley-minlength="9"
                                                   data-parsley-maxlength="11"
                                                   data-parsley-type="digits"
                                                   data-parsley-minlength-message="Minimum of 9 digits"
                                                   data-parsley-maxlength-message="Maximum of 11 digits"
                                                   data-parsley-type-message="This value should contain digits only"/>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="category_f" class="control-label">Category </label>
                                            <select class="form-control" v-model="filter.category" id="category_f"
                                                    name="category">
                                                <option value="SUPPORT">Support</option>
                                                <option value="TECH">Tech</option>
                                                <option value="ENQUIRY">Enquiry</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="fint_category_f" class="control-label">FINT Category </label><br>
                                            <select id="fint_category_f" class="form-control" v-model="filter.fint_category">
                                                <option value="BORROWER">Borrower</option>
                                                <option value="LENDER">Lender</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="priority_f" class="control-label">Priority </label><br>
                                            <p class="btn-group radio-group radio-group-success">
                                                <input id="r_normal_f" name="priority" value="NORMAL"
                                                       v-model="filter.priority"
                                                       type="radio">
                                                <label for="r_normal_f" class="btn">Normal</label>
                                                <input id="r_high_f" name="priority" value="HIGH"
                                                       v-model="filter.priority"
                                                       type="radio">
                                                <label for="r_high_f" class="btn">High</label>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="tags" class="control-label">Tags </label><br>
                                            <select class="form-control" v-model="filter.tags">
                                                @foreach ($tags as $tag)
                                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="switch switch-line">
                                                <input id="resolved_f" name="resolved" v-model="filter.resolved"
                                                       type="checkbox">
                                                <label for="resolved_f" class="control-label">Resolved</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-offset-2 col-sm-2">
                                        <a class="btn btn-default" @click.prevent="refreshEntries()">Refresh</a>
                                    </div>

                                    <div class="col-sm-offset-2 col-sm-2">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>s/n</th>
                    <th>Phone</th>
                    <th>Name</th>
                    <th>Feedback</th>
                    <th>Priority</th>
                    <th>Resolved</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="entry in entries">
                    <td>@{{entry.id}}</td>
                    <td>@{{entry.phone}}</td>
                    <td>@{{entry.name}}</td>
                    <td>@{{entry.feedback}}</td>
                    <td>@{{entry.priority}}</td>
                    <td>
                        <button @click="resolveEntry(entry.id)" class="btn btn-success btn-xs"
                                v-if="entry.resolved_by == null">Resolve
                        </button>
                        <button @click="unresolveEntry(entry.id)" class="btn btn-default btn-xs"
                                v-else>Unresolve
                        </button>
                        {{--<span v-else><i class="fa fa-check-circle-o" style="color: #133d55"></i></span>--}}
                    </td>
                    <td>@{{entry.date_opened | moment}}</td>
                    <td>
                        <button @click="showEntry(entry.id)" class="btn btn-primary btn-xs editBtn">Edit</button>
                    </td>
                </tr>
                </tbody>
            </table>

            <nav aria-label="pagination">
                <ul class="pager">
                    <li style="cursor: pointer;" :class="{ disabled: styles.prevDisabled}"
                        @click.prevent.stop="prevEntries()"><a>Previous</a>
                    </li>
                    <li style="cursor: pointer;" :class="{ disabled: styles.nextDisabled}"
                        @click.prevent.stop="nextEntries()"><a>Next</a>
                    </li>
                </ul>
            </nav>

        </div>
    </div>

</div>


<footer>
    <div class="text-center">&copy;FINT Technology Africa 2017</div>
</footer>

<script type="text/javascript" src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/nprogress.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/select2.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/axios.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/parsley.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/select2.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/toastr.min.js')}}"></script>

<script type="text/javascript" src="{{URL::asset('js/vue.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/vue-awesome.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/custom-components.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/custom.js')}}"></script>


</body>
</html>

