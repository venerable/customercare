Vue.component('select2', {
    props: ['options', 'value'],
    template: '<select class="form-control" multiple><slot></slot></select>',
    mounted: function () {
        var vm = this;
        $(this.$el)
        // init select2
            .select2({data: this.options})
            .val(this.value)
            .trigger('change')
            // emit event on change.
            .on('change', function () {
                vm.$emit('input', $(this).val())
            })
    },

    watch: {
        value: function (value) {
            // update value
            $(this.$el).push(value)
        },
        options: function (options) {
            // update options
            $(this.$el).empty().select2({data: options})
        },
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
})
