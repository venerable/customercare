var app = new Vue({
        el: '#app',

        data: {
            submitBtnName: 'Submit',
            edit: false,
            entry: {
                id: '',
                phone: '',
                name: '',
                fint_category: '',
                fint_id: '',
                feedback: '',
                category: '',
                priority: 'NORMAL',
                tags: [],
                resolved: false
            },
            filter: {
                phone: '',
                category: '',
                fint_category: '',
                priority: '',
                tags: [],
                resolved: false
            },
            entries: [],
            styles: {
                prevDisabled: true,
                nextDisabled: false
            },
            page: 1,
        },

        methods: {
            fetchEntries: function () {
                axios.get('entries', {
                    params: {page: this.page}
                }).then(function (response) {
                    NProgress.done(true);
                    this.entries = response.data.data;

                    if (response.data.prev_page_url === null) { // ...gotten to the first page
                        this.styles.prevDisabled = true;
                    }
                    if (response.data.next_page_url === null) { // ...gotten to the last page
                        this.styles.nextDisabled = true;
                    }
                }.bind(this)).catch(function (err) {
                    NProgress.done();
                    toastr.error('An error occured');
                }.bind(this));;
            },
            nextEntries: function () {
                if (!this.styles.nextDisabled) {
                    NProgress.start();
                    this.page++;
                    this.fetchEntries();
                    this.styles.prevDisabled = false;
                }
            },
            prevEntries: function () {
                if (!this.styles.prevDisabled) {
                    NProgress.start();
                    this.page--;
                    this.fetchEntries();
                    this.styles.nextDisabled = false;
                }
            },
            createEntry: function () {
                NProgress.start();
                axios.post('entry/store', this.entry).then(function (response) {
                    toastr.success('Feedback created successfully');
                    NProgress.done();
                    this.edit = false
                    this.entries.unshift(response.data.entry);
                    this.resetEntryData();
                }.bind(this)).catch(function (err) {
                    NProgress.done();
                    toastr.error('An error occured');
                }.bind(this));;
            },
            updateEntry: function (id) {
                NProgress.start();
                this.submitBtnName = 'Edit'
                axios.patch('entry/edit/' + id, this.entry).then(function () {
                    toastr.success('Feedback updated successfully');
                    this.edit = false;
                    this.resetEntryData();
                    this.submitBtnName = 'Submit';
                    this.fetchEntries();
                }.bind(this)).catch(function (err) {
                    NProgress.done();
                    toastr.error('An error occured');
                }.bind(this));;
            },
            resolveEntry: function (id) {
                NProgress.start();
                axios.patch('entry/resolve/' + id)
                    .then(function () {
                        this.fetchEntries();
                        toastr.success('Issue resolved successfully');
                    }.bind(this))
                    .catch(function (err) {
                        NProgress.done();
                        toastr.error('An error occured');
                    }.bind(this));
            },
            unresolveEntry: function (id) {
                NProgress.start();
                axios.patch('entry/unresolve/' + id).then(function () {
                    this.fetchEntries();
                    toastr.success('Issue unresolved successfully');
                }.bind(this)).catch(function (err) {
                    NProgress.done();
                    toastr.error('An error occured');
                }.bind(this));;
            },
            showEntry: function (id) {
                NProgress.start();
                axios.get('entry/' + id).then(function (response) {
                    NProgress.done();
                    this.entry.id = response.data.id;
                    this.entry.phone = response.data.phone;
                    this.entry.name = response.data.name;
                    this.entry.fint_id = response.data.fint_id;
                    this.entry.fint_category = response.data.fint_category;
                    this.entry.feedback = response.data.feedback;
                    this.entry.category = response.data.category;
                    this.entry.priority = response.data.priority;
                    this.entry.resolved = response.data.resolved_by ? true : false;

                    var tags = [];
                    for (var i = 0; i < response.data.tags.length; i++) {
                        tags.push(response.data.tags[i].id);
                    }
                    this.entry.tags = tags

                    this.edit = true
                }.bind(this)).catch(function (err) {
                    NProgress.done();
                    toastr.error('An error occured');
                }.bind(this));;
            },
            deleteEntry: function (id) {
                NProgress.start();
                axios.delete('entry/' + id);
                this.fetchEntries();
            },
            filterEntries: function () {
                NProgress.start();
                axios.get('entries/filter', {
                    params: this.filter
                }).then(function (response) {
                    NProgress.done();
                    this.entries = response.data;
                }.bind(this)).catch(function (err) {
                    NProgress.done();
                    toastr.error('An error occured');
                }.bind(this));;
            },
            refreshEntries: function () {
                NProgress.start();
                this.resetFilterData();
                this.fetchEntries();
            },
            resetEntryData: function () {
                this.entry.id = null;
                this.entry.phone = null;
                this.entry.name = null;
                this.entry.fint_id = null;
                this.entry.feedback = null;
                this.entry.category = 'SUPPORT';
                this.entry.priority = 'NORMAL';
                this.entry.tags = [];
                this.entry.resolved = false
            },
            resetFilterData: function () {
                this.filter.phone = null;
                this.filter.category = null;
                this.filter.priority = null;
                this.filter.fint_category = null;
                this.filter.tags = [];
                this.filter.resolved = false
            }
        },

        filters: {
            moment: function (date) {
                return moment(date).format('MMM Do, YYYY, h:mm A');
            },
            shorten: function (text) {
                if (text.length <= 40) {
                    return text.substring(0, 40);
                }
                return text.substring(0, 40) + '...';
            }
        },

        mounted: function () {
            toastr.options = {
                "closeButton": false,
                "newestOnTop": true,
                "positionClass": "toast-bottom-right",
                "timeOut": "3000"
            }
            $('#feedbackForm').parsley();
            $('#filter-form').parsley();
            this.fetchEntries();
            $('body').on('click', '.editBtn', function () {
                $('#tab1_a').tab('show');
            });


        }
    })
;
